+++
title = "Da Lin's Personal Website"


# The homepage contents
[extra]
lead = 'This is a personal website for <b>Da Lin</b>.'
url = "/docs/getting-started/introduction/"
url_button = "My Portfolios"

# Menu items
[[extra.menu.main]]
name = "Portfolios"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "Journey Begins Here"
content = 'I have always enjoyed plays games on my personal computer. That draws me into learning CS as major.'

[[extra.list]]
title = "First Line of Code ⚡️"
content = 'Under the guidence of my brother, I wrote my first line of code, "print("Hello, World!")", on a Macbook in an apple retail store.'

[[extra.list]]
title = "First Website"
content = "I had my first version of personal website merely in html and css, which is much uglier than this one. "

[[extra.list]]
title = "Internship"
content = "As a data analyst intern, I worked in EY to help perform fraud detection and build a prototype for their machine learning platform."

[[extra.list]]
title = "Researches"
content = "Supervised by Professor Shan Aman-Rana, who works at economics department in UVA, I craeted Django webite for studet survey purpose."

[[extra.list]]
title = "iOS Applications"
content = "With both experiences during undergraduate and graduate studies, I wish to build a iOS app for study abroad students."

+++
